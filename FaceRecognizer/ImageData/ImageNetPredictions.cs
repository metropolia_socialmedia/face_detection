﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceRecognizer.ImageData
{
    public class ImageNetPrediction
    {
        public float[] Score;

        public string PredictedLabelValue;
    }

    public class ImageNetWithLabelPrediction : ImageNetPrediction
    {
        public ImageNetWithLabelPrediction(ImageNetPrediction pred, string label)
        {
            Label = label;
            Score = pred.Score;
            PredictedLabelValue = pred.PredictedLabelValue;
        }

        public string Label;
    }
}
