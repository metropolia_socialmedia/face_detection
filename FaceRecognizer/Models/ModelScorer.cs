﻿using FaceRecognizer.ImageData;
using Microsoft.ML;
using Microsoft.ML.Core.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static FaceRecognizer.Model.ConsoleHelpers;
namespace FaceRecognizer.Estimator.Models
{
    public class ModelScorer
    {
        private readonly string modelLocation;
        private readonly MLContext mlContext;

        public ModelScorer(string modelLocation)
        {
            this.modelLocation = modelLocation;
            mlContext = new MLContext(1);
        }
        public void ClassifyImage(string dataUrl)
        {
            ConsoleWriteHeader("Loading model");
            Console.WriteLine($"Model loaded: {modelLocation}");
            ITransformer loadedModel;
            using(var f= new FileStream(modelLocation, FileMode.Open))
            {
                loadedModel = mlContext.Model.Load(f);

                var predictor = mlContext.Model.CreatePredictionEngine<ImageNetData, ImageNetPrediction>(loadedModel);
                ImageNetData data = new ImageNetData() { ImagePath = dataUrl, Label = "Minju" };
                var result = new { data, pred = predictor.Predict(data) };
                ConsoleWriteHeader("Making classifications");
                ConsoleWriteImagePrediction(dataUrl, result.pred.PredictedLabelValue, result.pred.Score.Max(), data.Label);
            }
        }
    }
}
