﻿using FaceRecognizer.Estimator.Models;
using System;
using System.Windows.Forms;
using static FaceRecognizer.Model.ConsoleHelpers;

namespace FaceRecognizer.Estimator
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            string modelscorerLocation = "";
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Select the Model zip file";
            ofd.Filter = "ZIP files (*.zip)|*.zip";
            ofd.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                modelscorerLocation = ofd.FileName;
                try
                {
                    var modelscorer = new ModelScorer(modelscorerLocation);
                    string sImgPath = "";
                    ofd.Filter = "Bitmaps|*.bmp|PNG files|*.png|JPEG files|*.jpg|GIF files|*.gif|TIFF files|*.tif|Image files|*.bmp;*.jpg;*.gif;*.png;*.tif";
                    ofd.Title = "Select Image to Compare...";
                    if (ofd.ShowDialog() == DialogResult.OK)
                    {
                        sImgPath = ofd.FileName;
                        modelscorer.ClassifyImage(sImgPath);
                    }
                }
                catch(Exception e)
                {
                    ConsoleWriteException(e.Message);
                }
                ConsolePressAnyKey();
            }
        }
    }
}
