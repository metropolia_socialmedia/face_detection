﻿using FaceRecognizer.Trainer.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static FaceRecognizer.Model.ConsoleHelpers;

namespace ImageNetPredictions.Trainer
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            using (var fdb = new FolderBrowserDialog())
            {
                fdb.Description = "Selecteer de folder met fotos:";
                if (fdb.ShowDialog() == DialogResult.OK && !string.IsNullOrWhiteSpace(fdb.SelectedPath))
                {
                    string sImgFolder = GetAbsolutePath(fdb.SelectedPath);
                    var tagsTsv = Path.Combine(sImgFolder, "imagenet.tsv");
                    var inceptionPb = Path.Combine(sImgFolder, "tensorflow_inception_graph.pb");
                    using (var sfd = new SaveFileDialog())
                    {
                        sfd.Title = "Save the Trained Model";
                        sfd.Filter = "Zip File (*.zip)|*.zip";
                        sfd.InitialDirectory = sImgFolder;
                        if (sfd.ShowDialog() == DialogResult.OK && !string.IsNullOrWhiteSpace(sfd.FileName))
                        {
                            try
                            {
                                var modelBuilder = new ModelBuilder(tagsTsv, sImgFolder, inceptionPb, sfd.FileName);
                                if (File.Exists(sfd.FileName))
                                {
                                    modelBuilder.BuildAndTrainMore();
                                }
                                else
                                {
                                    
                                    modelBuilder.BuildAndTrain();
                                }
                                
                            }
                            catch (Exception e)
                            {
                                ConsoleWriteException(e.Message);
                            }
                            Console.ReadKey();
                        }
                    }
                }
            }
        }
        public  static string GetAbsolutePath(string relativePath)
        {
            FileInfo _dataRoot = new FileInfo(typeof(Program).Assembly.Location);
            string assemblyFolderPath = _dataRoot.Directory.FullName;

            string fullPath = Path.Combine(assemblyFolderPath, relativePath);

            return fullPath;
        }
    }
}
